<?php

/**
 * @file
 * This file contains functions for preprocess custom defined theme hoooks
 */


function template_preprocess_hello_world_example(&$variables){
  $name_user = \Drupal::currentUser()->getAccountName();
  $email_user = \Drupal::currentUser()->getEmail();
  $id_user = \Drupal::currentUser()->id();

  $variables['name_user'] = $name_user;
  $variables['email_user'] = $email_user;
  $variables['id_user'] = $id_user;

  $variables['info_user'] ="Name = ". $name_user." Email = " .$email_user. " ID= ". $id_user;
}
