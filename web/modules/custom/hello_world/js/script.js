/**
 * @file
 * Simple JavaScript hello world file.
 */

(function ($, Drupal, settings) {

  "use strict";

  Drupal.behaviors.helloworld = {
    attach: function (context) {
      console.log(1+1);
    }
  }

})(jQuery, Drupal, drupalSettings);
