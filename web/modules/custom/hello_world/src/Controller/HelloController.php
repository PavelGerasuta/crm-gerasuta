<?php

namespace Drupal\hello_world\Controller;

use Drupal\Console\Bootstrap\Drupal;
use Drupal\Core\Controller\ControllerBase;

/**
* Defines HelloController class.
*/
class HelloController extends ControllerBase {

/**
* Display the markup.
*
* @return array
*   Return markup array.
*/
public function content() {

  // Подключение библиотеки. Состоит из двух частей.
  // Первое - название модуля/темы, второе - название библиотеки.
  $build['#attached']['library'][] = 'hello_world/vexjs';
  $build['#attached']['library'][] = 'hello_world/helloworld';
  $build['#attached']['library'][] = 'hello_world/block_header';


  $build['content'] = [
    '#theme' => 'hello_world_example',
    '#info_user' => "NULL",
  ];

  return $build;

}

}
