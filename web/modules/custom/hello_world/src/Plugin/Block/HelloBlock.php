<?php

namespace Drupal\hello_world\Plugin\Block;

use Drupal\Console\Bootstrap\Drupal;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Hello' Block.
 *
 * @Block(
 *   id = "hello_block",
 *   admin_label = @Translation("Hello block"),
 *   category = @Translation("Hello World"),
 * )
 */
class HelloBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
      $config = $this->getConfiguration();
      $user = \Drupal::currentUser()->getAccountName();
      $date = getdate();


      if (!empty($config['hello_block_data'])) {
        if ($config['hello_block_data'] == '1') {
          $toDate = 'Hello, World! '.$user.', today '.$date['year']. " ". $date['month']
            ." ". $date['yday'] ." ". $date['hours'] .":". $date['minutes'];
        }elseif ($config['hello_block_data'] == '2') {
          $toDate = 'Hello, World! '.$user.', today '.$date['year']. " ". $date['month'];
        }elseif ($config['hello_block_data'] == '3'){
          $toDate = 'Hello, World! '.$user.', today '. $date['hours'] .":". $date['minutes'];
        }
      }
      else {
        $toDate = \Drupal::time()->getRequestTime();
      }



    return [
      '#markup' => $this->t($toDate),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['hello_block_data'] = [
      '#type' => 'select',
      '#title' => $this->t('Choose time format'),
      '#description' => $this->t('Change view data format'),
      '#options' => [
        '1' => $this->t('All data'),
        '2' => $this->t('Year and month'),
        '3' => $this->t('Only time' ),
      ],
      '#default_value' => '1',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['hello_block_data'] = $form_state->getValue('hello_block_data');

  }



  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $default_config = \Drupal::config('hello_world.settings');
    return [
      'hello_block_name' => $default_config->get('hello.name'),
    ];
  }

}
